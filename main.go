package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"
)

func deleteArray(data []string, d int) []string {
	dataOut := make([]string, 0)
	for i := 0; i < d; i++ {
		dataOut = append(dataOut, data[i])
	}
	for i := d + 1; i < len(data); i++ {
		dataOut = append(dataOut, data[i])
	}
	return dataOut
}

func GetRandMatch(links []string, mails []string) map[string][]string {
	dataout := make(map[string][]string)
	for i := range links {
		if links[i] == "" {
			continue
		}
		dataout[links[i]] = make([]string, 0)
	}
	mailList := make([]string, 0)
	for i := range mails {
		if mails[i] == "" {
			continue
		}
		mailList = append(mailList, mails[i])
	}
	stopLoop := false
	for {
		for i := range dataout {
			if len(mailList) == 0 {
				stopLoop = true
				break
			}
			r := rand.Intn(len(mailList))
			mail := mailList[r]
			mailList = deleteArray(mailList, r)
			dataout[i] = append(dataout[i], mail)
		}
		if stopLoop {
			break
		}
	}

	return dataout
}

func main() {
	rand.Seed(time.Now().UnixNano())
	linkfile, err := ioutil.ReadFile("links.txt")
	if err != nil {
		fmt.Printf("Error to read file links.txt \n")
		return
	}
	mailfile, err := ioutil.ReadFile("mails.txt")
	if err != nil {
		fmt.Printf("Error to read file mails.txt \n")
		return
	}

	links := strings.Split(string(linkfile), "\n")
	mails := strings.Split(string(mailfile), "\n")

	data := GetRandMatch(links, mails)

	for i := range data {
		fmt.Printf("Link: %s\n", i)
		for i2 := range data[i] {
			if i2 == len(data[i])-1 {
				fmt.Printf("%s", data[i][i2])
			} else {
				fmt.Printf("%s, ", data[i][i2])
			}
		}
		fmt.Printf("\n\n")
	}

}
